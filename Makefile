prerequire:
	brew install pre-commit commitizen direnv
	pnpm install

post-install:
	pre-commit install --hook-type pre-commit --hook-type commit-msg --hook-type pre-push

commit:
	cz commit

check:
	pre-commit run --all-files

update:
	pre-commit autoupdate
