import { OpenTelemetrySDK, globalBootstrap } from '@gsm-core/gsm-foundation';
import { Logger } from '@nestjs/common';
import figlet from 'figlet';
import { AppModule } from './app/app.module';
async function bootstrap() {
  OpenTelemetrySDK.start();

  const globalPrefix = 'api';
  const moduleName = 'gsm-insurance';
  const app = await globalBootstrap({ module: AppModule, moduleName, globalPrefix });
  const host = process.env.HOST || '0.0.0.0';
  const port = process.env.PORT || 3000;
  await app.listen(port);

  Logger.log(figlet.textSync('GSM  Insurance!'));
  Logger.log(`🚀 Application is running on: http://${host}:${port}/${globalPrefix}`);
}

bootstrap();
