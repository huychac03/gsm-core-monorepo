import { Controller, Get, Query, Req } from '@nestjs/common';
import { ProxyResponse } from '../types/response.type';
import { AppService } from './app.service';
import { GoogleMapService } from './google-maps.service';

interface AutoCompleteEntities {
  language?: string;
  components?: string;
  radius?: number;
}
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly googleMapsService: GoogleMapService,
  ) {}

  @Get('/place/autocomplete/json')
  async getAutocomplete(
    @Query('input') input: string,
    @Query('location') location: string,
    @Query() query: AutoCompleteEntities,
  ) {
    const { language, components, radius } = query;
    const { data } = await this.googleMapsService.getAutocomplete({
      input,
      location,
      language,
      components,
      radius,
    });
    const response: ProxyResponse = {
      success: true,
      data,
    };
    return response;
  }

  @Get('/place/details/json')
  async getPlaceDetail(@Req() req) {
    const { query } = req;
    const { placeid, place_id, placeId } = query;
    const placeIdResult = placeId || place_id || placeid;
    const { data } = await this.googleMapsService.getPlaceDetails(placeIdResult);

    const response: ProxyResponse = {
      success: true,
      data,
    };
    return response;
  }

  @Get('/directions/json')
  async getDirections(@Req() req) {
    const { query } = req;
    const { language, ...other } = query;
    const { data } = await this.googleMapsService.getDirections({ language, other });

    const response: ProxyResponse = {
      success: true,
      data,
    };
    return response;
  }

  @Get('/place/findplacefromtext/json')
  async getFindPlaceFromText(@Req() req) {
    const { query } = req;
    const { ...queries } = query;
    const { data } = await this.googleMapsService.getFindPlaceFromText(queries);

    const response: ProxyResponse = {
      success: true,
      data,
    };
    return response;
  }

  @Get('/geocode/json')
  async getGeoCode(@Req() req) {
    const { query } = req;
    const { ...queries } = query;
    const { latlng } = query;
    const { data } = await this.googleMapsService.getGeoCode(latlng, queries);

    const response: ProxyResponse = {
      success: true,
      data,
    };
    return response;
  }

  @Get('/distancematrix/json')
  async getDistanceMatrix(@Req() req) {
    const { query } = req;
    const { ...queries } = query;
    const { data } = await this.googleMapsService.getDistanceMatrix(queries);

    const response: ProxyResponse = {
      success: true,
      data,
    };
    return response;
  }
}
