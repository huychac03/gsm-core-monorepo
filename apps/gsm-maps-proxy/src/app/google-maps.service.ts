/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosResponse } from 'axios';

@Injectable()
export class GoogleMapService {
  GOOGLE_MAPS_HOST: string;
  GOOGLE_MAPS_KEY: string;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService<EnvironmentVariables>,
  ) {
    this.GOOGLE_MAPS_HOST = this.configService.get('NX_GOOGLE_MAPS_HOST', { infer: true });
    this.GOOGLE_MAPS_HOST = this.configService.get('NX_GOOGLE_MAPS_KEY', { infer: true });
  }

  async getAutocomplete({
    input,
    location,
    language = 'vi',
    components = 'components',
    radius = 5000,
  }: {
    input: string;
    location: string;
    language?: string;
    components?: string;
    radius?: number;
  }): Promise<AxiosResponse<any>> {
    const url = `${this.GOOGLE_MAPS_HOST}/maps/api/place/autocomplete/json`;
    const params = {
      inputtype: 'textquery',
      language,
      input,
      components,
      location,
      radius,
      key: this.GOOGLE_MAPS_KEY,
    };
    return this.httpService.axiosRef.get(url, { params });
  }

  async getPlaceDetails(placeId: string): Promise<AxiosResponse<any>> {
    const url = `${this.GOOGLE_MAPS_HOST}/maps/api/place/details/json`;
    const params = {
      placeid: placeId,
      key: this.GOOGLE_MAPS_KEY,
    };
    return this.httpService.axiosRef.get(url, { params });
  }

  async getDirections({ language = 'vi-vn', other }: { language?: string; other: any }): Promise<AxiosResponse<any>> {
    const url = `${this.GOOGLE_MAPS_HOST}/maps/api/directions/json`;
    const params = {
      language,
      ...other,
      key: this.GOOGLE_MAPS_KEY,
    };
    return this.httpService.axiosRef.get(url, { params });
  }

  async getFindPlaceFromText(queries: any): Promise<AxiosResponse<any>> {
    const url = `${this.GOOGLE_MAPS_HOST}/maps/api/place/findplacefromtext/json`;
    const params = {
      ...queries,
      key: this.GOOGLE_MAPS_KEY,
    };
    return this.httpService.axiosRef.get(url, { params });
  }

  async getGeoCode(latlng: string, queries: any): Promise<AxiosResponse<any>> {
    const url = `${this.GOOGLE_MAPS_HOST}/maps/api/geocode/json`;
    const params = {
      ...queries,
      latlng,
      key: this.GOOGLE_MAPS_KEY,
    };
    return this.httpService.axiosRef.get(url, { params });
  }

  async getDistanceMatrix(queries: any): Promise<AxiosResponse<any>> {
    const url = `${this.GOOGLE_MAPS_HOST}/maps/api/distancematrix/json`;
    const params = {
      ...queries,
      key: this.GOOGLE_MAPS_KEY,
    };
    return this.httpService.axiosRef.get(url, { params });
  }
}
