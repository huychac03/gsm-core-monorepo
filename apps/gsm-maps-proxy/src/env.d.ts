interface EnvironmentVariables {
  NX_GOOGLE_MAPS_HOST: string;
  NX_GOOGLE_MAPS_KEY: string;
  NX_API_KEY: string;
}
