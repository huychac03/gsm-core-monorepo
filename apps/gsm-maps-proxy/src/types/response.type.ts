/* eslint-disable @typescript-eslint/no-explicit-any */
export interface ProxyResponse {
  success: boolean;
  data: any;
  message?: any;
}
