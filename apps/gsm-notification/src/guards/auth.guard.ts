import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    console.log('🚀 ~ file: auth.guard.ts:8 ~ AuthGuard ~ canActivate ~ request:', request.query);
    const header = request.headers['x-api-key'];
    return true;
  }
}
