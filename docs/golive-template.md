# Go-live Template

## Service

Service to release. Eg: GSM Backend for Global

## Changelog

Describe changelog for this release

## Release Date

Planning release date

## PIC

Who related to this release. Who support this release.

## Release Version

The version release

## Related Service(s)

Input related service(s)

## QC Passed Jira Ticket

The link and ID of Jira ticket that passed UAT by QC

For example: [JIRA-1234](https://jira.atlassian.com/browse/JIRA-12

## Release Script (For Technical Team)

The link to Jira that include changed for technical (configuration, change in the database,...)
