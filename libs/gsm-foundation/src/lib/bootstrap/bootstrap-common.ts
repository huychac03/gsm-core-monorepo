/* eslint-disable @typescript-eslint/no-explicit-any */
import compression from '@fastify/compress';
import helmet from '@fastify/helmet';
import { RequestMethod, VersioningType } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import morgan from 'morgan';
import { WinstonModule, utilities as nestWinstonModuleUtilities } from 'nest-winston';
import * as winston from 'winston';
import { AllExceptionsFilter } from './all-exception.filter';

export async function globalBootstrap({
  module,
  moduleName,
  globalPrefix,
}: {
  module: any;
  moduleName: string;
  globalPrefix: string;
}): Promise<NestFastifyApplication> {
  const app = await NestFactory.create<NestFastifyApplication>(module, new FastifyAdapter(), {
    logger: WinstonModule.createLogger({
      // options (same as WinstonModule.forRoot() options)
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(moduleName, {
              colors: true,
              prettyPrint: true,
            }),
          ),
        }),
        // other transports...
      ],
    }),
  });

  await app.register(compression);

  // TODO: Update CORS later
  app.enableCors({
    origin: '*',
    allowedHeaders: '*',
    methods: '*',
  });

  app.enableVersioning({
    defaultVersion: '2',
    type: VersioningType.URI,
  });

  app.register(
    helmet,
    // Example disables the `contentSecurityPolicy` middleware but keeps the rest.
    { contentSecurityPolicy: false },
  );
  app.use(morgan('combined'));
  app.useGlobalFilters(new AllExceptionsFilter());

  app.setGlobalPrefix(globalPrefix, {
    exclude: [{ path: 'health', method: RequestMethod.GET }],
  });

  if (process.env.NODE_ENV === 'development') {
    const config = new DocumentBuilder()
      .setTitle('GSM Insurance APIs')
      .setDescription('APIs collection for GSM')
      .setVersion('2.0')
      .addTag('gsm')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
  }

  return app;
}
