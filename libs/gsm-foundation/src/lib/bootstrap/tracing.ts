import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-proto';
import { HttpInstrumentation } from '@opentelemetry/instrumentation-http';
import { NestInstrumentation } from '@opentelemetry/instrumentation-nestjs-core';
import { WinstonInstrumentation } from '@opentelemetry/instrumentation-winston';
import { B3Propagator } from '@opentelemetry/propagator-b3';
import { Resource } from '@opentelemetry/resources';
import { api, NodeSDK } from '@opentelemetry/sdk-node';
import { SimpleSpanProcessor } from '@opentelemetry/sdk-trace-base';
import { NodeTracerProvider } from '@opentelemetry/sdk-trace-node';
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';

const provider = new NodeTracerProvider();
provider.register();

const otlpExporter = new OTLPTraceExporter({
  // url: `http://localhost:14268/api/traces`,
  url: process.env.NX_JAEGER_EXPORTER_URL,
});

// Set B3 Propagator
api.propagation.setGlobalPropagator(new B3Propagator());

export const OpenTelemetrySDK = new NodeSDK({
  resource: new Resource({
    [SemanticResourceAttributes.SERVICE_NAME]: `gsm-service`,
  }),
  spanProcessor: new SimpleSpanProcessor(otlpExporter),
  instrumentations: [
    new HttpInstrumentation(),
    new NestInstrumentation(),
    new WinstonInstrumentation({
      // Optional hook to insert additional context to log metadata.
      // Called after trace context is injected to metadata.
      logHook: (span, record) => {
        record['resource.service.name'] = provider.resource.attributes['service.name'];
      },
    }),
  ],
});

// gracefully shut down the SDK on process exit
process.on('SIGTERM', () => {
  OpenTelemetrySDK.shutdown()
    .then(
      () => console.log('SDK shut down successfully'),
      (err) => console.log('Error shutting down SDK', err),
    )
    .finally(() => process.exit(0));
});
