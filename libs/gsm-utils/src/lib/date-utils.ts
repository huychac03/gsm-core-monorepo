import dayjs, { Dayjs } from 'dayjs';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import isoWeek from 'dayjs/plugin/isoWeek';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import timezone from 'dayjs/plugin/timezone';
import updateLocale from 'dayjs/plugin/updateLocale';
import utc from 'dayjs/plugin/utc';
import weekOfYear from 'dayjs/plugin/weekOfYear';
import weekday from 'dayjs/plugin/weekday';

dayjs.extend(isSameOrBefore);
dayjs.extend(updateLocale);
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.extend(weekOfYear);
dayjs.extend(weekday);
dayjs.extend(isoWeek);
dayjs.extend(localizedFormat);
dayjs.updateLocale('en', {
  weekStart: 1,
});

export class DateUtils {
  static dayjs(date?: string | Dayjs | Date) {
    return dayjs(date).utc();
  }

  static getWeek() {
    return this.dayjs().week();
  }

  static getYear() {
    return this.dayjs().year();
  }

  static getDate() {
    return this.dayjs();
  }

  static getDateAsISOString(date?: string | Dayjs): string {
    return date ? this.dayjs(date).toISOString() : this.dayjs().toISOString();
  }

  static convertISOString(date?: string | Dayjs | Date) {
    return this.dayjs(date).toISOString();
  }

  static gapWeek(day1: string | Date | Dayjs, day2: string | Date | Dayjs) {
    const date1 = this.dayjs(day1);
    const date2 = this.dayjs(day2);
    return date2.diff(date1, 'week');
  }

  static isSameDate(date1: string, date2: string): boolean {
    const day1 = this.dayjs(date1);
    const day2 = this.dayjs(date2);
    return day1.isSame(day2, 'date');
  }

  static isSameOrBefore(date1: string | Date | Dayjs, date2: string | Date | Dayjs): boolean {
    const day1 = this.dayjs(date1);
    const day2 = this.dayjs(date2);
    return day1.isSameOrBefore(day2, 'second');
  }

  static isBefore(date1: string | Date | Dayjs, date2: string | Date | Dayjs): boolean {
    const day1 = this.dayjs(date1);
    const day2 = this.dayjs(date2);
    return day1.isBefore(day2, 'day');
  }

  static isSameWeekAndYear(date1: string, date2: string): boolean {
    const day1 = this.dayjs(date1);
    const day2 = this.dayjs(date2);

    const isSameWeek = day1.isSame(day2, 'week');
    const isSameYear = day1.isSame(day2, 'year');

    return isSameWeek && isSameYear;
  }

  static getMonday(date: string | Dayjs | Date) {
    return this.dayjs(date).weekday(0);
  }

  static getNextMonday(date: string | Dayjs | Date) {
    return this.dayjs(date).weekday(7);
  }

  static getLastMonday(date: string | Dayjs) {
    return this.dayjs(date).weekday(-7);
  }

  static getLastSunday(date: string | Dayjs) {
    return this.dayjs(date).weekday(-8);
  }

  static getSunday(date: string | Dayjs | Date) {
    return this.dayjs(date).weekday(6);
  }

  static addDay(date: string | Dayjs) {
    return this.dayjs(date).add(1, 'day');
  }

  static endOfDay(date: string | Dayjs) {
    return this.dayjs(date).endOf('day');
  }

  static startOfDay(date: string | Dayjs) {
    return this.dayjs(date).startOf('day');
  }
}
